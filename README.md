# GeekSessions

Sample Android project, showing some of Android's inter-app communication capabilities.

When an SMS message is received with the proper format (described bellow), an event creation page will be displayed, which can then be easily added to the system calendar. It is also possible to simulate the received message behavior in the main/entry application screen. 


## JSON SMS message format

    {
      "GEEKSESSION":
         {
              "title":"event title",
              "description":"event description",
              "where":"event location",
              "start":"yyyyMMddHHmm",
              "end":"yyyyMMddHHmm"
         }
    }

Every parameter is optional, except for the `"GEEKSESSION"` container. 


## Main concepts

Some of the core Android concepts referenced in this project are:

 - The application life-cycle and its components;
 - *Intent filters* and *broadcast receivers* (for intercepting SMS messages);
 - Implicit and explicit intents (opening the sample event page for the former, and creating a new event in the system calendar for the latter);
 - String *i18n* (portuguese localization included; with english language by default).
