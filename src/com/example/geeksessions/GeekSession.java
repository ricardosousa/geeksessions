/**
 * 
 */
package com.example.geeksessions;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONObject;
import android.annotation.SuppressLint;
import android.util.MalformedJsonException;


/**
 * @author RSousa
 *
 */
public class GeekSession implements Serializable
{

	
	private static final long	serialVersionUID	= 1402709399359988391L;
	private Date start,end;
	private String where,title,description;
	
	
	@SuppressLint("SimpleDateFormat")
	public GeekSession(String fromData) throws ParseException{
		
		final SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmm");
		
		try{
			JSONObject obj = new JSONObject(fromData);
			JSONObject session = obj.getJSONObject("GEEKSESSION");
			if(session==null)throw new MalformedJsonException("Missing required GeekSession JSON object");
			
			if(session.has("start"))
				start=formatter.parse(session.getString("start"));
			if(session.has("end"))
				end=formatter.parse(session.getString("end"));
			
			if(start==null)
				start=end==null?new Date():new Date(end.getTime()-60*60*1000);
		
			if(end==null)
				end=new Date(start.getTime()+60*60*1000);
			
			where=session.optString("where", "");
			title=session.optString("title", "Geek Session");
			description=session.optString("description", "");
			
		}catch(Exception e){
			throw new ParseException(e.getMessage(), 0);
		}
	}


	
	public String getDescription() {
		return description;
	}


	
	public void setDescription( String description ) {
		this.description= description;
	}


	
	public Date getStart() {
		return start;
	}


	
	public Date getEnd() {
		return end;
	}


	
	public String getWhere() {
		return where;
	}


	
	public String getTitle() {
		return title;
	}
	
	
}
