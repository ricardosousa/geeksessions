
package com.example.geeksessions;


import java.text.SimpleDateFormat;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Events;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class NewEventActivity extends Activity
{


	@SuppressLint("SimpleDateFormat")
	@Override
	protected void onCreate( Bundle savedInstanceState ) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_event);
		// Show the Up button in the action bar.
		setupActionBar();

		Button btn= (Button) findViewById(R.id.addEvent);
		TextView title= (TextView) findViewById(R.id.eventTitle);
		TextView desc= (TextView) findViewById(R.id.eventDesc);
		TextView date= (TextView) findViewById(R.id.eventDate);

		btn.setVisibility(View.GONE);


		

		if (getIntent().getExtras()!=null){
			final GeekSession message= (GeekSession) getIntent().getExtras()
					.get("smsdata");
			if (message!=null){

				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd (HH:mm)");
				
				title.setText(message.getTitle());
				desc.setText(message.getDescription());
				date.setText(formatter.format(message.getStart()));

				btn.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick( View v ) {
						addEvent(message);
					}
				});
				btn.setVisibility(View.VISIBLE);
			}


		}else{
			Toast.makeText(this, "could not retreive extras", Toast.LENGTH_LONG).show();
		}

	}


	private void addEvent( GeekSession message ) {

		Intent intent= new Intent(Intent.ACTION_INSERT)
				.setType("vnd.android.cursor.item/event")
				.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME,
							message.getStart().getTime())
				.putExtra(CalendarContract.EXTRA_EVENT_END_TIME,
							message.getEnd().getTime())
				.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, false)
				.putExtra(Events.TITLE, message.getTitle())
				.putExtra(Events.DESCRIPTION, message.getDescription())
				.putExtra(Events.EVENT_LOCATION, message.getWhere())
				.putExtra(Events.AVAILABILITY, Events.AVAILABILITY_BUSY)
				.putExtra(Events.ACCESS_LEVEL, Events.ACCESS_PRIVATE);
		startActivity(intent);

	}


	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu( Menu menu ) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.other, menu);
		return true;
	}


	@Override
	public boolean onOptionsItemSelected( MenuItem item ) {
		switch(item.getItemId()){
			case android.R.id.home:
				// This ID represents the Home or Up button. In the case of this
				// activity, the Up button is shown. Use NavUtils to allow users
				// to navigate up one level in the application structure. For
				// more details, see the Navigation pattern on Android Design:
				//
				// http://developer.android.com/design/patterns/navigation.html#up-vs-back
				//
				NavUtils.navigateUpFromSameTask(this);
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
